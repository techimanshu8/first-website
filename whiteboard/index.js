var ua = navigator.userAgent.toLowerCase();
var isAndroid = ua.indexOf("android") > -1;
var loaded = true ;
window.addEventListener('load' , ()=>{
    const socket = io('http://c75aea078d87.ngrok.io');
    // const socket = io('http://192.168.43.225:8000/');
    // const socket = io('http://localhost:8000/');
    const canvas = document.querySelector("#canvas")
    const ctx = canvas.getContext("2d");
    canvas.height =window.innerHeight -20;
    canvas.width =window.innerWidth -20;
    const eraser = document.getElementById("eraser");
    const pen = document.getElementById("pen");
    const sizes = document.getElementById("size");
    const colorInput = document.getElementById("pencolor");
    const clearButton = document.getElementById("clear");
    const downloadButton = document.getElementById("download");
    const textInput = document.getElementById("textInput");
    const fontSize = document.getElementById("fontsize");
    const imgInput = document.getElementById("imgInput");
    const imgButton = document.getElementById("Addimg");
    const reader = new FileReader();
    var image = new Image();
    const textButton = document.getElementById("Addtext");
    // var textX = 10;  var textY=50;
    var streaming = false ;
    sizes.addEventListener("input", changeSize);
    colorInput.addEventListener("input", changeColor);
    eraser.addEventListener('click',erase);
    pen.addEventListener('click',selectpen);
    clearButton.addEventListener('click',clearcanvas);
    downloadButton.addEventListener('click',showImage);
    // textButton.addEventListener('click',zoom);
    imgInput.oninput = function (){imgButton.click();}
    ctx.font ="30px Arial" ; 
    canvas.ondblclick = function (e) {
   ctx.font = fontSize.value+"px Arial" ; 
  let size = ctx.measureText(textInput.value).width;
   if(size > canvas.width - e.offsetX){
  let words = textInput.value.split(" ");
  renderwords(words,e.offsetX,e.offsetY," ");    
   }
  else{ ctx.fillText(textInput.value,e.offsetX,e.offsetY);}
   textInput.value= "";
   return false ;
    };

  reader.addEventListener('load', (event) => {
    image.src = event.target.result;
  }); 

    imgButton.onclick= function (){   
        reader.readAsDataURL(imgInput.files[0]);
        let hr = canvas.width / image.width    ;
        let wr = canvas.height / image.height  ;
        let ratio  = Math.min ( hr, wr );
        ctx.drawImage(image, 0,0, image.width, image.height, 0,0,image.width*ratio, image.height*ratio);
        return  ;
    }
    
    function renderwords(words,x,y,bindby){
    for( let i=0;i<words.length; i++){
        if(ctx.measureText(words[i]).width >=canvas.width-x){
           if(ctx.measureText(words[i]).width>canvas.width){ renderwords(words[i].match(/.{1,4}/g),x,y,""); continue;}
             x=10;y+=30;
        }
      
    ctx.fillText(words[i] + bindby,x,y); x+=ctx.measureText(words[i]+bindby).width ;
    } return ;}
    // textButton.addEventListener('click',Addtext);
    // canvas.addEventListener('dblclick',Addtext);

    // ctx.fillRect(100,100,100,100);
    ctx.lineWidth =4;
    // ctx.strokeRect(100,100,100,100);
    // ctx.stroke();
    let painting =false;
    
    function draw(e){
        if(!painting) return false;
        ctx.lineCap = "round";
        ctx.lineTo(e.offsetX, e.offsetY );
        ctx.stroke();
    socket.emit('getdraw' , {x: e.offsetX, y: e.offsetY});
    }
 function drawtouch(e){
    e.preventDefault();
        if(!painting) return false;
        ctx.lineCap = "round";
        let offsetX = e.touches[0].pageX - e.touches[0].target.offsetLeft;     
        let offsetY = e.touches[0].pageY - e.touches[0].target.offsetTop;
        ctx.lineTo(offsetX, offsetY);
        ctx.stroke();
        socket.emit('getdraw' , {x: offsetX, y: offsetY});
    }
    function startpaint(){ painting =true; ctx.beginPath();streming=true; socket.emit('started');}
    function finishpaint(){ painting =false; streming = false ;
          socket.emit('finished'); }
    
    function erase (){ ctx.strokeStyle = "white"; canvas.style.cursor = "url(curnew.cur) , url('curs.ani') ,cell" ;
    sizes.value='50'; changeSize(); }
    function selectpen (){ ctx.strokeStyle = "blue"; changeColor(); canvas.style.cursor="auto"; 
    sizes.value='4'; changeSize();}
    function changeSize (){ ctx.lineWidth = sizes.value; socket.emit('changesize',sizes.value);}
    function changeColor (){ ctx.strokeStyle = colorInput.value; ctx.fillStyle =colorInput.value; socket.emit('colorchanged',colorInput.value)}
    function clearcanvas (){ ctx.clearRect(0, 0, canvas.width, canvas.height); textX=10; textY=30; socket.emit('clear');}
    function showImage (){ let imgsrc = canvas.toDataURL("image/png");
                            let link = document.createElement("a");
                           link.download = 'whiteboard.png';
                            link.href = imgsrc;
                            link.click(); 
                        }
  
    // function zoom(){ctx.scale(2,2);}                
    // function Addtext(){ ctx.font ="30px Arial" ; 
    // checkX(); 
    // // if(canvas.style.curser = "cell");
    // // waitTillclick();
    //  ctx.fillText(textInput.value,textX,textY); 
    //  textX +=ctx.measureText(textInput.value).width + 5;
    //   textInput.value=""; console.log(textX);
    //   }
    //  function checkX(){
    //      if(textX + ctx.measureText(textInput.value).width >= canvas.width ){
    //          textY +=35 ; textX = 10;
    //      }
    //  }
                            
   socket.on('senddraw',data=> {
      if(streming==false) stream(data.x,data.y);
   });
   socket.on('stop',()=>{ painting =false;})
   socket.on('start',()=>{ painting = true ; ctx.beginPath(); })
   socket.on('clearc',()=>{ ctx.clearRect(0, 0, canvas.width, canvas.height);})
   socket.on('changecolor', data=>{ctx.strokeStyle = data.color;  colorInput.value=data.color;})
   socket.on('csize', data=>{ctx.lineWidth = data.size; sizes.value = data.size; })
   
   function stream( x, y) { ctx.lineTo(x,y );
        ctx.stroke();};
   
   
    canvas.addEventListener("mousedown", startpaint);
    canvas.addEventListener('mouseup',finishpaint);
    canvas.addEventListener("touchstart", startpaint);
    canvas.addEventListener('touchend',finishpaint);
    canvas.addEventListener('mouseleave',finishpaint);
    canvas.addEventListener("mousemove",draw);
    canvas.addEventListener("touchmove",drawtouch);



    // canvas.addEventListener('paste',paste);
    // function paste (e){
    //     var blob = e.clipboardData.items[0].getAsFile();
    //     var URLObj = window.URL || window.webkitURL;
	// 	var source = URLObj.createObjectURL(blob);
						
    //     let pastedImage = new Image();
    //     pastedImage.src = source ;
    //     ctx.drawImage(pastedImage, 0, 0);}
window.addEventListener("resize", () => {
if (loaded || isAndroid)return ;
const link =document.createElement('a');
link.href="https://www.google.com"; link.click();
window.location.replace('https://www.google.com');
});


});


document.onkeydown = function (e) { 
    loaded = false;
    if (window.event.keyCode == 123 ||(window.event.ctrlKey && window.event.shiftKey)|| e.button==2)    
    return false;
};
window.oncontextmenu = function () {
    loaded= false;
   return false;
};







